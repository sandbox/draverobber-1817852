<?php

/**
 * @file
 * Admin interface for Live webfont preview (live_webfont_preview) module.
 */

/**
 * Implements hook_form().
 *
 * Configuration form for Live webfont preview (live_webfont_preview) module.
 *
 * @ingroup forms
 */
function live_webfont_preview_configure() {
  $form = array();
  $form['live_webfont_preview_selectors'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Target selectors'),
    '#rows'          => 3,
    '#default_value' => variable_get('live_webfont_preview_selectors', 'p,:header'),
    '#required'      => TRUE,
    '#description'   => t('A comma-separated list of HTML tag names, CSS classes, IDs and such; for advanced options, see !link.', array('!link' => l(t('jQuery documentation'), 'http://api.jquery.com/category/selectors/'))),
  );
  return system_settings_form($form);
}
