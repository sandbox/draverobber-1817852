Live webfont preview (live_webfont_preview) module

SUMMARY

This module lets you quickly test how your site would look with different
webfonts provided by @font-your-face module, by allowing you to switch between
enabled fonts on-the fly, without reloading the page.

HOW TO USE IT

Upon enabling the module, a '@' anchor appears near the top left corner of
target elements; hovering over it reveals a drop-down menu where you can choose
a font (provided you have enabled some).
Near the bottom right corner of the screen, there is a button to toggle anchor
display on/off. You can also toggle anchor display by pressing '2' on the
keyboard.
Live webfont preview is available to users with 'use live webfont preview'
permission. To configure the module, you need 'administer @font-your-face'
permission.

PREREQUISITES

Live webfont preview depends on @font-your-face
(http://drupal.org/project/fontyourface).
It also requires a JavaScript-capable browser.

CONFIGURATION

-- TARGET SELECTORS

Page elements that will get font selection menus; you can use HTML tag names,
CSS classes, IDs and such here; for advanced options, consult jQuery
documentation (http://api.jquery.com/category/selectors/).

KNOWN ISSUES

When changing the font results in changing vertical size of an element, it can
occasionally break the positioning of anchors below it.

Anchor positioning does not work well with tables in Chrome.
