/**
 * @file
 * JS for the Live webfont preview (live_webfont_preview) module.
 */

jQuery(document).ready(function($) {
  // Get target selectors and unset the variable from the settings array.
  var selectors = Drupal.settings.lwp.selectors;
  delete Drupal.settings.lwp.selectors;
  // Get font names.
  var fonts = Drupal.settings.lwp;
  // Construct drop-down menu.
  var dropDown = '<ul class="lwp-box"><li class="lwp-anchor">@</li>';
  var allClasses = '';
  $.each(fonts, function(index, value) {
    dropDown += '<li class="lwp-item ' + index + '">' + fonts[index] + '</li>';
    allClasses += ' ' + index;
  });
  dropDown += '<li class="lwp-item lwp-active">-- ' + Drupal.t('default') + ' --</li>';
  dropDown += '</ul>';
  // Insert drop-down before non-empty, visible target elements.
  $(selectors).filter(':parent').not('.element-invisible').each(function(i) {
    $(this).before(dropDown);
    $(this).parent(i).addClass('lwp');
    // Calculate and apply vertical offset.
    var topOffset = $(this).offset().top - $(this).parent(i).offset().top + parseFloat($(this).css('paddingTop'));
    $(this).prev(i).css('top', topOffset);
    // Calculate and apply horizontal offset.
    var leftOffset = $(this).offset().left - $(this).parent(i).offset().left + parseFloat($(this).css('paddingLeft')) - 20;
    $(this).prev(i).css('left', leftOffset);
    // Copy element's line-height attribute to the anchor. Then add some
    // negative bottom margin to compensate for it. All this is still part of
    // positioning.
    var lineHeight = parseFloat($(this).css('lineHeight'));
    $(this).prev(i).children().first().css('line-height', lineHeight + 'px');
    $(this).prev(i).children().first().css('margin-bottom', -0.5 * lineHeight + 8 + 'px');
  });
  // Process click on drop-down menu.
  $('.lwp-box li.lwp-item').click(function(i) {
    // The "font class" is the one which is not 'lwp-active'.
    var font = $(this).attr('class').replace('lwp-active', '');
    // Add 'lwp-active' to the clicked item, remove from its siblings.
    $(this).addClass('lwp-active');
    $(this).siblings().removeClass('lwp-active');
    // Remove previous font class from the target element, add new one.
    // The target element next to the parent of the menu item.
    $(this).parent(i).next().removeClass(allClasses);
    $(this).parent(i).next().addClass(font);
  });
  // Process click on the toggle.
  $('.lwp-toggle').click(function(i) {
    $(this).toggleClass('lwp-toggle-hidden');
    $('body').toggleClass('lwp-hidden');
  });
  // Process '2' key (on numpad, too - to avoid misunderstanding).
  $(document).keydown(function(e) {
    if (e.keyCode == 50 || e.keyCode == 98) {
      $('.lwp-toggle').toggleClass('lwp-toggle-hidden');
      $('body').toggleClass('lwp-hidden');
    }
  });
});
